using System;

namespace ConsumptionTax
{
    public class 消費税
    {
        public static int 消費税額(int 税抜金額)
        {
            var 税額 = 税抜金額 * 0.10m;
            return (int) Math.Floor(税額);
        }
    }
}