namespace ConsumptionTax
{
    public class Dummy
    {
        // coverlet.collector によるカバレッジの取得は
        // カバレッジ対象にできるクラスファイルが複数ないと
        // GitLab が認識できる形式にならない。
        // そのためのダミーファイル。

        public void DummyMethod()
        {
        }
    }
}