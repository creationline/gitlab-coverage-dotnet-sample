using Xunit;

namespace ConsumptionTax.Tests
{
    public class 消費税のテスト
    {
        [Fact]
        public void 税抜100円の消費税額は10円()
        {
            var actual = 消費税.消費税額(100);

            Assert.Equal(10, actual);
        }
    }
}